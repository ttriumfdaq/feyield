/********************************************************************\

  Name:         experim.h
  Created by:   ODBedit program

  Contents:     This file contains C structures for the "Experiment"
                tree in the ODB and the "/Analyzer/Parameters" tree.

                Additionally, it contains the "Settings" subtree for
                all items listed under "/Equipment" as well as their
                event definition.

                It can be used by the frontend and analyzer to work
                with these information.

                All C structures are accompanied with a string represen-
                tation which can be used in the db_create_record function
                to setup an ODB structure which matches the C structure.

  Created on:   Wed Feb 11 09:56:44 2009

\********************************************************************/

#define EXP_EDIT_DEFINED

typedef struct {
  DWORD     sis_mode;
  BOOL      pedestals_run;
  INT       pedestal_events;
  INT       pedestal_period;
} EXP_EDIT;

#define EXP_EDIT_STR(_name) char *_name[] = {\
"[.]",\
"SIS_mode = DWORD : 0",\
"Pedestals run = BOOL : y",\
"Pedestal events = INT : 0",\
"Pedestal period = INT : 50",\
"",\
NULL }

#ifndef EXCL_VME

#define VME_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} VME_COMMON;

#define VME_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 11",\
"Trigger mask = WORD : 2048",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 8",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 1000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] isysfe.triumf.ca",\
"Frontend name = STRING : [32] fevme",\
"Frontend file name = STRING : [256] fevme.cxx",\
"",\
NULL }

#endif

#ifndef EXCL_SISHISTORY

#define SISHISTORY_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} SISHISTORY_COMMON;

#define SISHISTORY_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 3",\
"Trigger mask = WORD : 8",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 257",\
"Period = INT : 1000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] isysfe.triumf.ca",\
"Frontend name = STRING : [32] fevme",\
"Frontend file name = STRING : [256] fevme.cxx",\
"",\
NULL }

#define SISHISTORY_SETTINGS_DEFINED

typedef struct {
  INT       clockchannel;
  double    clockfrequency;
  INT       adchannel;
  char      adtalk[32];
  INT       mixchannel;
  char      mixtalk[32];
} SISHISTORY_SETTINGS;

#define SISHISTORY_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"ClockChannel = INT : 16",\
"ClockFrequency = DOUBLE : 50000000",\
"ADChannel = INT : 1",\
"ADTalk = STRING : [32] Shoo",\
"MixChannel = INT : 1",\
"MixTalk = STRING : [32] Mix",\
"",\
NULL }

#endif

#ifndef EXCL_ADC

#define ADC_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} ADC_COMMON;

#define ADC_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 2",\
"Trigger mask = WORD : 4",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 257",\
"Period = INT : 1",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] isysfe.triumf.ca",\
"Frontend name = STRING : [32] fevme",\
"Frontend file name = STRING : [256] fevme.cxx",\
"",\
NULL }

#endif

#ifndef EXCL_SCALER

#define SCALER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} SCALER_COMMON;

#define SCALER_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 5",\
"Trigger mask = WORD : 32",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 257",\
"Period = INT : 1000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] isysfe.triumf.ca",\
"Frontend name = STRING : [32] fevme",\
"Frontend file name = STRING : [256] fevme.cxx",\
"",\
NULL }

#endif

#ifndef EXCL_PULSER

#define PULSER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} PULSER_COMMON;

#define PULSER_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 7",\
"Trigger mask = WORD : 128",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 100",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] isysfe.triumf.ca",\
"Frontend name = STRING : [32] fevme",\
"Frontend file name = STRING : [256] fevme.cxx",\
"",\
NULL }

#define PULSER_SETTINGS_DEFINED

typedef struct {
  double    period[2];
} PULSER_SETTINGS;

#define PULSER_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"Period = DOUBLE[2] :",\
"[0] 0",\
"[1] 0",\
"",\
NULL }

#endif

#ifndef EXCL_LATCH

#define LATCH_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} LATCH_COMMON;

#define LATCH_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 9",\
"Trigger mask = WORD : 512",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 257",\
"Period = INT : 1000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] isysfe.triumf.ca",\
"Frontend name = STRING : [32] fevme",\
"Frontend file name = STRING : [256] fevme.cxx",\
"",\
NULL }

#endif

#ifndef EXCL_SISPULSER

#define SISPULSER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} SISPULSER_COMMON;

#define SISPULSER_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 7",\
"Trigger mask = WORD : 128",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 10000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] isysfe.triumf.ca",\
"Frontend name = STRING : [32] fevme",\
"Frontend file name = STRING : [256] fevme.cxx",\
"",\
NULL }

#endif

