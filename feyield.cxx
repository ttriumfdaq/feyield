/********************************************************************

  Name:         fecim.cxx
  Created by:   P. Kunz

  Contents:     yield softare frontend (yields)

  2010/01/22 13:22

*******************************************************************/

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
#include <assert.h>
#undef HAVE_ROOT
#undef USE_ROOT
#include "midas.h"
#include "mfe.h"
#include "evid.h"

#define  CAENCOMM_ACCESS 1
#define  HAVE_SIS3820
#define V785_BUFFER 32
#define SIS3820_CHANNELS 32

#ifdef MVME_ACCESS
#include "mvmestd.h"
#elif CAENCOMM_ACCESS
extern "C" {
#include <CAENComm.h>
}
#endif

extern "C" {
#include "v792.h"
#include "sis3820drv.h"
#include "sis3820.h"
}

/* make frontend functions callable from the C framework */

/*-- Globals -------------------------------------------------------*/
  struct timeval now, before;
 // int iEventBuffer = 0;
 //  WORD* pdata;
 
#ifdef MVME_ACCESS
  MVME_INTERFACE *myvme;
  MVME_INTERFACE *gVme = 0;

#elif CAENCOMM_ACCESS
  uint32_t CAEN_USBVME_BASE = 0x52100000;
  CAENComm_ErrorCode sCAEN;
  int device_handle;
#endif


  uint16_t gIoreg = 0;
  
  int gAdc1base    = 0x10000000;
  int gMcsBase[] = { 0x38000000, 0 };
  int sismode;
  int iDataBufferSize;
  int iBins;
  int iMCSrun;
  bool bUserBit = TRUE;
  DWORD *data;
  int iBusyCount = 0;
  double iBusyPerSecondAverage = 0.0;
  

/* The frontend name (client name) as seen by other MIDAS clients   */
   char const  *frontend_name = "feyield";
/* The frontend file name, don't change it */
   char const *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
   BOOL frontend_call_loop = FALSE;

/* a frontend status page is displayed with this frequency in ms */
   INT display_period = 000;

/* maximum event size produced by this frontend */
   INT max_event_size = 500*1024;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
   INT max_event_size_frag = 256*1024;

/* buffer size to hold events */
   INT event_buffer_size = 256*1024*1024;

   DWORD iScint = 0;
   //DWORD iCycle = 0;
   DWORD iPrevTimeStamp = 0;
   
  extern INT run_state;
  extern HNDLE hDB;

/*-- Function declarations -----------------------------------------*/
  INT frontend_init();
  INT frontend_exit();
  INT begin_of_run(INT run_number, char *error);
  INT end_of_run(INT run_number, char *error);
  INT pause_run(INT run_number, char *error);
  INT resume_run(INT run_number, char *error);
  INT frontend_loop();
  
  INT read_adc_event(char *pevent, INT off);
  INT read_scaler_event(char *pevent, INT off);
  INT read_pulser_event(char *pevent, INT off);
  INT read_latch_event(char *pevent, INT off);
  INT read_sispulser_event(char *pevent, INT off);
  INT read_mcs_event(char *pevent, INT off);
  INT read_vme_event(char *pevent, INT off);

/*-- Bank definitions ----------------------------------------------*/

/*-- Equipment list ------------------------------------------------*/
  
  EQUIPMENT equipment[] = {

    {"SisMCS",            /* equipment name */
     {EVID_MCS, (1<<EVID_MCS),  /* event ID, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,         /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      FALSE,                   /* enabled */
      RO_RUNNING|RO_ODB,      /* read only when running */
      100,                   /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* whether to log history */
      "", "", "",}
     ,
     read_mcs_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    }
    ,
    {"ADC",                   /* equipment name */
     {EVID_ADC, (1<<EVID_ADC),  /* event ID, trigger mask */
      "SYSTEM",               /* event buffer */
//      EQ_POLLED,              // equipment type 
                    EQ_MULTITHREAD,         /* equipment type */
   //EQ_PERIODIC,              // equipment type 
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING|RO_ODB,      /* read only when running */
      1000,                   /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* whether to log history */
      "", "", "",}
     ,
     read_adc_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    }
    ,
    {"Scaler",                   /* equipment name */
     {EVID_SCALER, (1<<EVID_SCALER),  /* event ID, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,             /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      FALSE,                   /* enabled */
      RO_RUNNING|RO_ODB,      /* read only when running */
      1000,                   /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* whether to log history */
      "", "", "",}
     ,
     read_scaler_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    }
    ,
    {"Pulser",                   /* equipment name */
     {EVID_PULSER, (1<<EVID_PULSER),  /* event ID, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,              /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      FALSE,                   /* enabled */
      RO_RUNNING,             /* read only when running */
      100,                    /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* don't log history */
      "", "", "",}
     ,
     read_pulser_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    }
    ,
    {"Latch",                   /* equipment name */
     {EVID_LATCH, (1<<EVID_LATCH),  /* event ID, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,              /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      FALSE,                   /* enabled */
      RO_RUNNING|RO_ODB,      /* when to read */
      1000,                   /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* whether to log history */
      "", "", "",}
     ,
     read_latch_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    }
    ,
    {"SisPulser",               /* equipment name */
     {EVID_PULSER, (1<<EVID_PULSER), /* event ID, trigger mask */
      "SYSTEM",                 /* event buffer */
      EQ_PERIODIC,              /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      FALSE,                   /* enabled */
      RO_RUNNING,             /* when to read */
      10000,                  /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* don't log history */
      "", "", "",}
     ,
     read_sispulser_event,          /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    }    
    ,
    {""}} ;

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

#ifdef MVME_ACCESS
#elif CAENCOMM_ACCESS
#endif

void encodeU32(char*pdata,uint32_t value)
{
  pdata[0] = (value&0x000000FF)>>0;
  pdata[1] = (value&0x0000FF00)>>8;
  pdata[2] = (value&0x00FF0000)>>16;
  pdata[3] = (value&0xFF000000)>>24;
}

void xusleep(int usec)
{
  struct timeval tv;
  tv.tv_sec = 0;
  tv.tv_usec = usec;
  select(0, NULL, NULL, NULL, &tv);
}

#include "utils.cxx"

int enable_trigger()
{
#ifdef HAVE_SIS3820
  for (int i=0; gMcsBase[i]!=0; i++)
#ifdef MVME_ACCESS
    sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_KEY_OPERATION_ENABLE,0);
#elif CAENCOMM_ACCESS
  sCAEN = CAENComm_Write32(device_handle, gMcsBase[i]+SIS3820_KEY_OPERATION_ENABLE,0);
#endif
#endif
 return 0;
}

int disable_trigger()
{
#ifdef HAVE_SIS3820
  for (int i=0; gMcsBase[i]!=0; i++)
#ifdef MVME_ACCESS
    sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_KEY_OPERATION_DISABLE,0);
#elif CAENCOMM_ACCESS
  sCAEN = CAENComm_Write32(device_handle, gMcsBase[i]+SIS3820_KEY_OPERATION_DISABLE,0);
#endif
#endif
  return 0;
}

/*-- VME setup     -------------------------------------------------*/

INT init_vme_modules()
{
#ifdef HAVE_SIS3820
  
  int iLNEsrc = 2;
  int prescale;
  int iLNEPrescale;
  
  iMCSrun = 0;
  
  for (int i=0; gMcsBase[i]!=0; i++)
    {
#ifdef MVME_ACCESS
      sis3820_Reset(gVme,gMcsBase[i]);
#elif CAENCOMM_ACCESS
      sCAEN = CAENComm_Write32(device_handle, gMcsBase[i]+SIS3820_KEY_RESET,0);
#endif
      sismode = odbReadUint32("/experiment/edit on start/SIS_mode",0,2);
      printf("SIS mode %i \n",sismode);
      iDataBufferSize = odbReadInt("/experiment/edit on start/Data_Buffer_Size",0,0x1000);
      printf("data buffer size %x \n",iDataBufferSize);
      data = (DWORD*)malloc(iDataBufferSize);
      
      switch (sismode)
	{
    case 0:
      printf("SIS mode 0: external LNE\n");
#ifdef MVME_ACCESS
      sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_LNE_PRESCALE,0);
      sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_OPERATION_MODE,
	SIS3820_CLEARING_MODE|
	SIS3820_MCS_DATA_FORMAT_32BIT|
	SIS3820_LNE_SOURCE_CONTROL_SIGNAL|
	SIS3820_FIFO_MODE|
	SIS3820_OP_MODE_MULTI_CHANNEL_SCALER|
	SIS3820_CONTROL_INPUT_MODE1|
	SIS3820_CONTROL_OUTPUT_MODE1);
#elif CAENCOMM_ACCESS
      sCAEN = CAENComm_Write32(device_handle, gMcsBase[i]+SIS3820_LNE_PRESCALE, 0);
      sCAEN = CAENComm_Write32(device_handle, gMcsBase[i]+SIS3820_OPERATION_MODE,
	SIS3820_CLEARING_MODE|
	SIS3820_MCS_DATA_FORMAT_32BIT|
	SIS3820_LNE_SOURCE_CONTROL_SIGNAL|
	SIS3820_FIFO_MODE|
	SIS3820_OP_MODE_MULTI_CHANNEL_SCALER|
	SIS3820_CONTROL_INPUT_MODE1|
	SIS3820_CONTROL_OUTPUT_MODE1);
#endif
      break;
      
    case 1:
      prescale = odbReadUint32("/experiment/edit on start/SIS_LNE_prescale",0,40);
      
      printf("SIS mode 1: internal LNE, prescale %d\n", prescale);
      
      // LNE prescale 40 is for max VME rate 30 Mbytes/sec
#ifdef MVME_ACCESS
      sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_LNE_PRESCALE,prescale);
      sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_OPERATION_MODE,
	SIS3820_CLEARING_MODE|
	SIS3820_MCS_DATA_FORMAT_32BIT|
	SIS3820_LNE_SOURCE_INTERNAL_10MHZ|
	SIS3820_FIFO_MODE|
	SIS3820_OP_MODE_MULTI_CHANNEL_SCALER|
	SIS3820_CONTROL_INPUT_MODE1|
	SIS3820_CONTROL_OUTPUT_MODE1);
#elif CAENCOMM_ACCESS
      sCAEN = CAENComm_Write32(device_handle, gMcsBase[i]+SIS3820_LNE_PRESCALE, prescale);
      sCAEN = CAENComm_Write32(device_handle, gMcsBase[i]+SIS3820_OPERATION_MODE,
	SIS3820_CLEARING_MODE|
	SIS3820_MCS_DATA_FORMAT_32BIT|
	SIS3820_LNE_SOURCE_INTERNAL_10MHZ|
	SIS3820_FIFO_MODE|
	SIS3820_OP_MODE_MULTI_CHANNEL_SCALER|
	SIS3820_CONTROL_INPUT_MODE1|
	SIS3820_CONTROL_OUTPUT_MODE1);
#endif
      break;
      
    case 2:
      iLNEsrc = odbReadInt("/experiment/edit on start/LNE source",0,2);
      
      printf("SIS mode 2: LNE source ADC event timing branch %d\n", iLNEsrc);
      
#ifdef MVME_ACCESS
      // sis3820_Status(gVme,gMcsBase[i]);
      sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_LNE_CHANNEL_SELECT,iLNEsrc);  // ADC event timing branch channel
      // SIS3820 operation mode: input mode 6: clear all counters
      sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_OPERATION_MODE,
	//  SIS3820_CLEARING_MODE|
	//SIS3820_MCS_DATA_FORMAT_32BIT|
	SIS3820_NON_CLEARING_MODE|
	//SIS3820_LNE_SOURCE_INTERNAL_10MHZ|
	//SIS3820_FIFO_MODE|
	// SIS3820_SDRAM_MODE|
	// SIS3820_OP_MODE_MULTI_CHANNEL_SCALER|
	//SIS3820_LNE_SOURCE_CONTROL_SIGNAL|
	SIS3820_CONTROL_INPUT_MODE2|
	SIS3820_CONTROL_INPUT_MODE4|
	SIS3820_CONTROL_OUTPUT_MODE1|
	SIS3820_LNE_SOURCE_CHANNEL_N
	);
      sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_CONTROL_STATUS,CTRL_REFERENCE_CH1_ENABLE);
      //sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_KEY_OPERATION_ENABLE,SIS3820_KEY_OPERATION_ENABLE);
#elif CAENCOMM_ACCESS
      uint32_t a;
      sCAEN = CAENComm_Read32(device_handle, gMcsBase[i]+SIS3820_LNE_CHANNEL_SELECT, &a);
      printf("dh: 0x%x sCAEN:%d Read32: add:0x%x  data %d \n", device_handle, sCAEN,gMcsBase[i]+SIS3820_LNE_CHANNEL_SELECT, &a);
      sCAEN = CAENComm_Write32(device_handle, gMcsBase[i]+SIS3820_LNE_CHANNEL_SELECT, iLNEsrc);
      printf("dh: 0x%x sCAEN:%d Write32: 0x%x \n", device_handle, sCAEN,gMcsBase[i]+SIS3820_LNE_CHANNEL_SELECT);
      sCAEN = CAENComm_Write32(device_handle, gMcsBase[i]+SIS3820_OPERATION_MODE,
	SIS3820_NON_CLEARING_MODE|
	SIS3820_CONTROL_INPUT_MODE2|
	SIS3820_CONTROL_INPUT_MODE4|
	SIS3820_CONTROL_OUTPUT_MODE1|
	SIS3820_LNE_SOURCE_CHANNEL_N
	);
      printf("dh: 0x%x sCAEN:%d Write32: 0x%x \n", device_handle, sCAEN,gMcsBase[i]+SIS3820_LNE_CHANNEL_SELECT);
      //  SIS3820_CLEARING_MODE|
      //SIS3820_MCS_DATA_FORMAT_32BIT|
      //SIS3820_LNE_SOURCE_INTERNAL_10MHZ|
      //SIS3820_FIFO_MODE|
      // SIS3820_SDRAM_MODE|
      // SIS3820_OP_MODE_MULTI_CHANNEL_SCALER|
      //SIS3820_LNE_SOURCE_CONTROL_SIGNAL|
#endif
      break;
      
    case 3:       
      iLNEsrc = odbReadInt("/experiment/edit on start/LNE source",0,2);
      
      printf("SIS mode 3: LNE source ADC event ... inhibit counting & LNE at tape move %d\n", iLNEsrc);
      
#ifdef MVME_ACCESS
      // sis3820_Status(gVme,gMcsBase[i]);
      sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_LNE_CHANNEL_SELECT,iLNEsrc);  // ADC event timing branch channel
      sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_OPERATION_MODE,
			    //  SIS3820_CLEARING_MODE|
			    //SIS3820_MCS_DATA_FORMAT_32BIT|
			    SIS3820_NON_CLEARING_MODE|
			    //SIS3820_LNE_SOURCE_INTERNAL_10MHZ|
			    //SIS3820_FIFO_MODE|
			    // SIS3820_SDRAM_MODE|
			    // SIS3820_OP_MODE_MULTI_CHANNEL_SCALER|
			    //SIS3820_LNE_SOURCE_CONTROL_SIGNAL|
			    SIS3820_CONTROL_INPUT_MODE2|
			    SIS3820_CONTROL_OUTPUT_MODE1|
			    SIS3820_LNE_SOURCE_CHANNEL_N
			    );
      
      sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_CONTROL_STATUS,CTRL_REFERENCE_CH1_ENABLE);
      // sis3820_RegisterWrite(gVme,gMcsBase[0],SIS3820_COUNTER_CLEAR,0xFFFFFFFF);  // clear counters 1-32
      sis3820_RegisterWrite(gVme,gMcsBase[i],0x214,0xF00); //ch9-16 veto inhibit
      //sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_KEY_OPERATION_ENABLE,SIS3820_KEY_OPERATION_ENABLE);
#elif CAENCOMM_ACCESS
      sCAEN = CAENComm_Write32(device_handle, gMcsBase[i]+SIS3820_LNE_CHANNEL_SELECT, iLNEsrc);
      sCAEN = CAENComm_Write32(device_handle, gMcsBase[i]+SIS3820_OPERATION_MODE,
			       SIS3820_NON_CLEARING_MODE|
			       SIS3820_CONTROL_INPUT_MODE2|
			       SIS3820_CONTROL_OUTPUT_MODE1|
			       SIS3820_LNE_SOURCE_CHANNEL_N
			       //  SIS3820_CLEARING_MODE|
			       //SIS3820_MCS_DATA_FORMAT_32BIT|
			       //SIS3820_LNE_SOURCE_INTERNAL_10MHZ|
			       //SIS3820_FIFO_MODE|
			       // SIS3820_SDRAM_MODE|
			       // SIS3820_OP_MODE_MULTI_CHANNEL_SCALER|
			       //SIS3820_LNE_SOURCE_CONTROL_SIGNAL
			       );
      sCAEN = CAENComm_Write32(device_handle, gMcsBase[i]+SIS3820_CONTROL_STATUS, CTRL_REFERENCE_CH1_ENABLE);
      sCAEN = CAENComm_Write32(device_handle, gMcsBase[i]+0x214,0xF00); //ch9-16 veto inhibit
#endif
      break;
      
    case 4:
      iLNEsrc = odbReadInt("/experiment/edit on start/LNE source",0,3);
      iBins =  odbReadInt("/experiment/edit on start/SIS3820_ACQUISITION_PRESET",0,1024);
      iLNEPrescale =  odbReadInt("/experiment/edit on start/SIS3820_LNE_PRESCALE",0,2000);
      printf("SIS mode 4: Decay mode: LNE source Ch4 ... inhibit counting & LNE at tape move %d\n", iLNEsrc);
      printf("/experiment/edit on start/SIS3820_LNE_PRESCALE %d\n", iBins);
      
#ifdef MVME_ACCESS
      // sis3820_Status(gVme,gMcsBase[i]);
      // sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_LNE_CHANNEL_SELECT,iLNEsrc);  // LNE channel
      //sis3820_RegisterWrite(myvme,sis3820_base,SIS3820_COPY_DISABLE,0xFFFFFFF0); // channels 1-4 copied to fifo
      sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_ACQUISITION_PRESET,iBins);
      sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_LNE_PRESCALE,iLNEPrescale-1);
      sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_OPERATION_MODE,
			    SIS3820_CLEARING_MODE|
			    SIS3820_MCS_DATA_FORMAT_32BIT|
			    SIS3820_LNE_SOURCE_INTERNAL_10MHZ|
			    // SIS3820_LNE_SOURCE_CHANNEL_N|
			    SIS3820_FIFO_MODE|
			    //SIS3820_SDRAM_MODE|
			    SIS3820_OP_MODE_MULTI_CHANNEL_SCALER|
			    SIS3820_CONTROL_INPUT_MODE2|
			    SIS3820_CONTROL_OUTPUT_MODE1
			    );
      
      sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_CONTROL_STATUS,CTRL_REFERENCE_CH1_ENABLE);
      sis3820_RegisterWrite(gVme,gMcsBase[i],0x214,0xF00); //ch9 - ch16 veto inhibit
      //sis3820_RegisterWrite(gVme,gMcsBase[i],SIS3820_KEY_OPERATION_ENABLE,SIS3820_KEY_OPERATION_ENABLE);
#elif CAENCOMM_ACCESS
      sCAEN = CAENComm_Write32(device_handle, gMcsBase[i]+SIS3820_ACQUISITION_PRESET, iBins);
      sCAEN = CAENComm_Write32(device_handle, gMcsBase[i]+SIS3820_LNE_PRESCALE, iLNEPrescale-1);
      sCAEN = CAENComm_Write32(device_handle, gMcsBase[i]+SIS3820_OPERATION_MODE,
			       SIS3820_CLEARING_MODE|
			       SIS3820_MCS_DATA_FORMAT_32BIT|
			       SIS3820_LNE_SOURCE_INTERNAL_10MHZ|
			       SIS3820_FIFO_MODE|
			       SIS3820_OP_MODE_MULTI_CHANNEL_SCALER|
			       SIS3820_CONTROL_INPUT_MODE2|
			       SIS3820_CONTROL_OUTPUT_MODE1
			       // SIS3820_SDRAM_MODE|
			       // SIS3820_LNE_SOURCE_CHANNEL_N|
			       );
#endif
      break;
    }
}
#endif
#ifdef HAVE_V792
   WORD  threshold[32];
#ifdef MVME_ACCESS
   v792_SingleShotReset(gVme,gAdc1base);
   //v792_IntEnable(gVme, gAdc1base, 1);
   v792_Setup(gVme,gAdc1base, 1);
#elif CAENCOMM_ACCESS
   sCAEN = CAENComm_Write16(device_handle, gAdc1base, 1);
   // v792_Setup mode 1 is empty
#endif
   
   threshold[0] = 0x6;
  for (int k=1;k<32;k++) 
  {
    threshold[k] = 0x1ff;
  }
#ifdef MVME_ACCESS
  v792_ThresholdWrite(gVme,gAdc1base, threshold);
  v792_DataClear(gVme,gAdc1base);
  v792_Status(gVme,gAdc1base);
  int   cmode;
  mvme_get_dmode(gVme, &cmode);
  mvme_set_dmode(gVme, MVME_DMODE_D32);
#elif CAENCOMM_ACCESS
// Threshold not done yet

// Data clear
  sCAEN = CAENComm_Write16(device_handle, gAdc1base+V792_BIT_SET2_RW, 0x4);
  sCAEN = CAENComm_Write16(device_handle, gAdc1base+V792_BIT_CLEAR2_WO, 0x4);
// Status Not done yet

#endif

#endif
  return SUCCESS;
}

EQUIPMENT* findEquipment(const char*name)
{
  for (int i=0; equipment[i].name[0] != 0; i++)
    if (strcmp(equipment[i].name, name) == 0)
      return &equipment[i];
  cm_msg(MINFO, frontend_name, "Cannot find equipment %s", name);
  abort();
}

/*-- Global variables ----------------------------------------------*/

static int gHaveRun        = 0;
static int gRunNumber      = 0;
static int gIsPedestalsRun = 0;
static int gMaxAdcEvents   = 0;

static double gPulserPeriod[8];

static EQUIPMENT* gAdcEquipmentPtr = 0;
static EQUIPMENT* gSisPulserEquipmentPtr = 0;

static int gSisBufferOverflow = 0;
/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
  int status;

  setbuf(stdout,NULL);
  setbuf(stderr,NULL);

  //  cm_enable_watchdog(0);

  gAdcEquipmentPtr       = findEquipment("ADC");
  gSisPulserEquipmentPtr = findEquipment("SisPulser");

#ifdef MVME_ACCESS
  status = mvme_open(&gVme,0);
  status = mvme_set_am(gVme, MVME_AM_A24_ND);
#elif CAENCOMM_ACCESS
  sCAEN = CAENComm_OpenDevice(CAENComm_USB, 0, 0, 0, &device_handle);
  printf("CAEN OpenDevice:%d %d %d\n", device_handle, sCAEN, CAENComm_USB);
  if (sCAEN != CAENComm_Success) {
    cm_msg(MERROR, "Iseg","Cannot access V1718 at this base: 0x%x", CAEN_USBVME_BASE);
    return FE_ERR_HW;
  }
#endif
  printf(" device_handle:%x\n", device_handle);
  status = init_vme_modules();
  gettimeofday(&before,NULL);
  if (status != SUCCESS)
    return status;

  disable_trigger();

  printf("frontend_init done!\n");

  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  gHaveRun = 0;
  disable_trigger();

#ifdef MVME_ACCESS
  mvme_close(gVme);
#elif CAENCOMM_ACCESS
  sCAEN = CAENComm_CloseDevice(device_handle);
#endif

  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  gRunNumber = run_number;
  gHaveRun = 1;
  gIsPedestalsRun = odbReadBool("/experiment/edit on start/Pedestals run");

  printf("begin run: %d, is pedestals run: %d\n",run_number,gIsPedestalsRun);

  al_reset_alarm("sis_overflow");
  gSisBufferOverflow = 0;

  int status = init_vme_modules();
  if (status != SUCCESS)
    return status;

  iMCSrun = 0;
  enable_trigger();
return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
  gHaveRun = 0;
  printf("end run %d\n",run_number);
  disable_trigger();
  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  gHaveRun = 0;
  gRunNumber = run_number;
  disable_trigger();
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  gHaveRun = 1;
  gRunNumber = run_number;
  enable_trigger();
  return SUCCESS;
}

static int gVmeIsIdle = 0;

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
  if (gVmeIsIdle) {
    cm_yield(7);
  }
  cm_yield(7);
  return SUCCESS;
}

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Interrupt configuration ---------------------------------------*/
INT interrupt_configure(INT cmd, INT source, PTYPE adr)
{
  //assert(!"Not implemented");
    switch (cmd) {
    case CMD_INTERRUPT_ENABLE:
      // mvme_interrupt_enable(gVme, int level, int vector, void *info);
       break;
    case CMD_INTERRUPT_DISABLE:
       break;
    case CMD_INTERRUPT_ATTACH:
       break;
    case CMD_INTERRUPT_DETACH:
       break;
    }
    return SUCCESS;

}

/*-- Event readout -------------------------------------------------*/

int read_v792(int base,const char*bname,char*pevent,int nchan)
{
  uint32_t w;
  int i,m;
  int iCSR2 = 0;
  int iCSR1 = 0;
  int wcount = 0;
  //DWORD data[10000];
//  DWORD counter = 0;
  DWORD* pdata;
  DWORD evtData[256];
  DWORD val;
  static double fDeadTime = 0;
  gettimeofday(&now,NULL);

  double td = (now.tv_sec - before.tv_sec) + 0.000001*(now.tv_usec - before.tv_usec);
          //printf("td = %f\n",td);

  fDeadTime = fDeadTime + td;
/* Event counter */
 // v792_EvtCntRead(gVme, base, &counter);
  
  /*
  if(counter >= 0xFFFFFF)
  {
    v792_EvtCntReset(gVme, base);
    v792_EvtCntRead(gVme, base, &counter);
    iEventCount = 0;
  }
  */
  /* Read Event */
  
//  if (counter > iEventCount)
  if (1)
  {
#ifdef MVME_ACCESS
    iCSR2 = v792_CSR2Read(gVme, base)  & 0x6; // buffer status bits: full 100, empty 010
    iCSR1 = v792_CSR1Read(gVme, base); // buffer status bits: full 100, empty 010
#elif CAENCOMM_ACCESS
    sCAEN = CAENComm_Read16(device_handle, gAdc1base+V792_CSR2_RO, (uint16_t *) &iCSR2);
    iCSR2 &= 0x6;
    sCAEN = CAENComm_Read16(device_handle, gAdc1base+V792_CSR1_RO, (uint16_t *) &iCSR1);
#endif
    
    if(iCSR1 & 0x4) // ADC busy
      {
	iBusyCount++;
	//fDeadTime = 0;
	iBusyPerSecondAverage = iBusyCount / fDeadTime;
	printf("ADC busy ... CSR1 = %x  busy count = %i  busy interval = %f  average = %f\n"
	       ,iCSR1,iBusyCount,fDeadTime, iBusyPerSecondAverage);
        //sleep(1);
      }
    if(iCSR2 & 0x4) // buffer full
      {
	printf("Buffer full ... CSR2 = %x  td = %f\n",iCSR2,td);
        //sleep(1);
      }
    //printf("polling ADC  td = %f\n",td);
    //printf("polling MCS .... user bit = %x\n",(sis3820_RegisterRead(gVme,gMcsBase[0],SIS3820_CONTROL_STATUS) & 0x10000000)>>28); 
    
    m = SIS3820_CHANNELS;
    // create ADCS bank
    memset(data,0,iDataBufferSize); // clear data array
    
#ifdef MVME_ACCESS
    status = sis3820_ShadowRead(gVme, gMcsBase[0], data, SIS3820_CHANNELS);
#elif CAENCOMM_ACCESS
    int32_t nw;
    sCAEN = CAENComm_BLTRead(device_handle, gMcsBase[0]+SIS3820_COUNTER_SHADOW_CH1, data, SIS3820_CHANNELS*sizeof(uint32_t), &nw); 
#endif
    bk_create(pevent, bname, TID_DWORD, (void **) &pdata);
    
    while(!(iCSR2 & 0x2))  // while buffer not empty
      {
	
        memset(evtData,0,256*sizeof(DWORD)); // clear data array
#ifdef MVME_ACCESS
        v792_EventRead(gVme, base, evtData, &wcount);
        iCSR2 = v792_CSR2Read(gVme, base)  & 0x6;
#elif CAENCOMM_ACCESS
#endif
	
        for (i=0; i < wcount; i++)
	  {
	    w = evtData[i];
	    if (((w>>24)&0x7) != 0) continue; // check data word format: bit 24-26 = 000
	    //	    chan = (w>>16)&0x1F;
	    val  = (w&0x1FFF);
	    data[m] = val;
	  }
        m++;
      }
      //data[8] = iCycle;  
      
      memcpy(pdata,data,(nchan+SIS3820_CHANNELS)*4);
      pdata += SIS3820_CHANNELS+nchan;
      bk_close(pevent, pdata);
      
      //bk_close(pevent, data+SIS3820_CHANNELS+nchan);
      gettimeofday(&before,NULL);
  }

  //printf("pulser = %i  counts = %i  cps = %f  ADC events = %i\n",data[3],data[2],((double)data[2]/(double)data[3]*10000),iEventCount);

  return wcount;
}

static const int kNumSisModules    =  1;   // number of SIS modules
static const int kSisChanPerModule = 32;   // number of channels in each SIS module
static const int kNumSisChannels = kNumSisModules*kSisChanPerModule; // total number of SIS channels
static uint32_t  gSumMcsEvents[kNumSisModules]; // sum the number of events
static uint32_t  gMaxMcs[kNumSisChannels];  // max value of SIS channels
static uint64_t  gSumMcs[kNumSisChannels];  // sum SIS channels
static uint32_t  gSaveMcs[kNumSisChannels]; // sampled SIS data

static uint32_t wc = 0;

static double timeDiff(const struct timeval&t1,const struct timeval&t2)
{
  return (t1.tv_sec - t2.tv_sec) + 0.000001*(t1.tv_usec - t2.tv_usec);
}

static struct timeval gSisLastRead[kNumSisModules];

static int have_SIS_data(int i)
{
  struct timeval now;

#ifdef MVME_ACCESS
  int haveData = sis3820_DataReady(gVme,gMcsBase[i]);
#elif CAENCOMM_ACCESS
  int haveData;
  sCAEN = CAENComm_Read32(device_handle, gMcsBase[i]+SIS3820_FIFO_WORDCOUNTER, (uint32_t *)&haveData);
#endif
  if (haveData == (-1))
    haveData = 0;

  int minread  = 1000*32;

  gettimeofday(&now,NULL);

  double td =  timeDiff(now, gSisLastRead[i]);

  //  printf("sis: %d, td: %f, haveData: %d\n",i,td,haveData);

  if (td < 1.0 && haveData < minread)
    return 0;

  //int toRead = minread;
  //int toRead = 128*1024/4;
  int toRead = max_event_size/4 - 5000;

  if (toRead > haveData)
    toRead = haveData;

  if (0)
    if (toRead & 0x1F)
      printf("odd available data: %d (%d)\n",toRead,toRead&0x1F);

  // make sure we always read in multiples of 32 words
  toRead = toRead & 0xFFFFFFE0;

  if (toRead == 0)
    return 0;

  return toRead;
}

static int read_SIS(char* pevent, int isis, int toRead32)
{
  assert(isis >= 0);
  assert(isis < kNumSisModules);

  //if (toRead32*4 < 20000)
  //  return 0;
  //
  //toRead32 = 32*1;

  int used = bk_size(pevent);
  int free = max_event_size - used;

  if (toRead32*4 > free)
    {
      //printf("read_SIS: buffer used %d, free %d, toread %d\n", used, free, toRead32*4);
      toRead32 = (free-10*1024)/4;
      toRead32 = toRead32 & 0xFFFFFFE0;

      if (toRead32 <= 0)
        return 0;
    }

  int maxDma = 256*1024;
  if (toRead32*4 > maxDma)
    toRead32 = maxDma/4;

  gettimeofday(&gSisLastRead[isis], NULL);

  /* create data bank */
  uint32_t *pdata32;

  char bankname[] = "MCS0";
  bankname[3] = '0' + isis; 
  bk_create(pevent, bankname, TID_DWORD, (void **) &pdata32);

  //printf("read %d words\n",toRead32);

#ifdef MVME_ACCESS
  int rd = sis3820_FifoRead(gVme,gMcsBase[isis],pdata32,toRead32);
#elif CAENCOMM_ACCESS
  int rd;
  sCAEN = CAENComm_BLTRead(device_handle, gMcsBase[isis]+SIS3820_FIFO_BASE, pdata32, toRead32*sizeof(int32_t), &rd);
#endif

  wc += toRead32*4;

#if 1
    printf("sis3820 data: 0x%x 0x%x 0x%x 0x%x\n",pdata32[16],pdata32[17],pdata32[18],pdata32[19]);
    printf("sis3820 data: rd: %d,",rd);
    for (int i=0; i<toRead32; i++)
      printf(" 0x%x",pdata32[i]);
    printf("\n");
#endif
  
  bk_close(pevent, pdata32 + rd);
  
  int numEvents = toRead32/kSisChanPerModule;
  uint32_t *mptr = pdata32;
  int offset = isis*kSisChanPerModule;
  for (int ievt=0; ievt<numEvents; ievt++) {
    gSumMcsEvents[isis]++;
    for (int i=0; i<kSisChanPerModule; i++)
      {
	uint32_t v = *mptr++;
	gSumMcs[offset+i] += v;
	gSaveMcs[offset+i] = v;
	if (v > gMaxMcs[offset+i])
	  gMaxMcs[offset+i] = v;
      }
  }
  
  return 1;
}

INT read_scaler_event(char *pevent, INT off)
{
  return 0;
}

INT read_pulser_event(char *pevent, INT off)
{
  static double gNextTime[8];

  struct timeval now;
  gettimeofday(&now,NULL);
  double t = now.tv_sec + 0.000001*now.tv_usec;

  uint32_t mask = 0;
  for (int i=1; i<8; i++)
    if (gPulserPeriod[i] > 0)
      if (t > gNextTime[i]) {
	if (gNextTime[i] == 0)
	  gNextTime[i] = t;
	gNextTime[i] += gPulserPeriod[i];
	//printf("pulse %d!\n",i);
	mask |= (1<<i);
      }
  return 0;
}

INT read_latch_event(char *pevent, INT off)
{
  uint16_t latch = 0;
  /* init bank structure */
  bk_init32(pevent);

  int *pdata;
  bk_create(pevent, "LAT0", TID_INT, (void **) &pdata);

  for (int i=0; i<16; i++)
    *pdata++ = (latch & (1<<i))?1:0;
  bk_close(pevent, pdata);

  return bk_size(pevent);
}

INT read_sispulser_event(char *pevent, INT off)
{
  //printf("Pulse SIS3820 NIM output\n");
#ifdef HAVE_SIS3820

#ifdef MVME_ACCESS
  sis3820_RegisterWrite(gVme,gMcsBase[0],SIS3820_CONTROL_STATUS,1<<0);
  // issue a PCI read to flush posted PCI writes
  uint32_t d1 = sis3820_RegisterRead(gVme,gMcsBase[0],SIS3820_CONTROL_STATUS);
  //usleep(1);
  sis3820_RegisterWrite(gVme,gMcsBase[0],SIS3820_CONTROL_STATUS,1<<16);
  // issue a PCI read to flush posted PCI writes
  uint32_t d2 = sis3820_RegisterRead(gVme,gMcsBase[0],SIS3820_CONTROL_STATUS);
#elif CAENCOMM_ACCESS
  sCAEN = CAENComm_Write32(device_handle, gMcsBase[0]+SIS3820_CONTROL_STATUS, 1<<0);
  uint32_t d1;
  sCAEN = CAENComm_Read32(device_handle, gMcsBase[0]+SIS3820_CONTROL_STATUS, &d1);
  sCAEN = CAENComm_Write32(device_handle, gMcsBase[0]+SIS3820_CONTROL_STATUS, 1<<16);
  uint32_t d2;
  sCAEN = CAENComm_Read32(device_handle, gMcsBase[0]+SIS3820_CONTROL_STATUS, &d2);
#endif
  return 0*(d1+d2);
#else
  return 0;
#endif
}

INT read_adc_event(char *pevent, INT off)
{
#ifndef HAVE_V792
  return 0;
#endif

#ifdef MVME_ACCESS
  int haveData = v792_DataReady(gVme,gAdc1base);
#elif CAENCOMM_ACCESS
  int haveData; 
  sCAEN = CAENComm_Read16(device_handle, gAdc1base+V792_CSR1_RO, (uint16_t *) &haveData);
#endif

  //v792_Status(gVme,gAdc1base);
  //printf("read adc event, have data: %d\n", haveData);

  if (!haveData)
    return 0;

  if (gMaxAdcEvents > 0)
    {
      printf("max events: %d\n",gMaxAdcEvents);
      gMaxAdcEvents--;
      if (gMaxAdcEvents == 0)
	{
	  cm_msg(MINFO, frontend_name, "Enough ADC pedestals events, requesting end of run");
	  cm_transition(TR_STOP, gRunNumber, NULL, 0, TR_ASYNC, 1);
	}
    }

  // init bank structure 
  bk_init32(pevent);
  
#ifdef MVME_ACCESS
  read_v792(gAdc1base,"ADC",pevent,V785_BUFFER);
#elif CAENCOMM_ACCESS
#endif

  /*
  if (v792_DataReady(gVme,gAdc1base))
    {
      if (gIsPedestalsRun)
	{
	  gAdcEquipmentPtr->last_called = 0;
	}
      else
	{
	  printf("read_adc_event: ADC gate is double pulsing!\n");
	  v792_SingleShotReset(gVme,gAdc1base);
	}
    }
  */
  // For every ADC gate, increase the spill number by one
  /*
    int status;
  int beamspill;
  HNDLE hdir = 0;
  HNDLE hkey;
  
  char* beamspilldir="/equipment/ADC/Variables/Spill Number";
  
  if(!gIsPedestalsRun)
  {
    beamspill = odbReadInt(beamspilldir);
    beamspill++;
  
    status = db_find_key(hDB,hdir, beamspilldir, &hkey);
    if (status != SUCCESS)
      {
        cm_msg(MERROR, frontend_name, "Cannot create \'%s\', db_find_key() status %d", beamspilldir, status);
        return false;
      }
    else
      {
        status = db_set_data_index(hDB, hkey, &beamspill, sizeof(beamspill), 0, TID_INT);
        if (status != SUCCESS)
          {
            cm_msg(MERROR, frontend_name, "Cannot write \'%d\' of type %d to odb, db_set_data_index() status %d", beamspill, 0, TID_INT, status);
            return false;
          }  
        //printf("Spill %d\n",beamspill);
      }
  }
  */
  return bk_size(pevent);
}


INT read_mcs_event(char *pevent, INT off)
{
#ifdef HAVE_SIS3820
// check user bit 1 ... not active disable SIS3820
if(sismode == 4)
{
#ifdef MVME_ACCESS
  if ((!(sis3820_RegisterRead(gVme,gMcsBase[0],SIS3820_CONTROL_STATUS) & 0x10000000)>>28) && (iMCSrun == 0))
#elif CAENCOMM_ACCESS
#endif
  {
    printf("Waiting for user bit 1\n"); 
    iMCSrun = 1;
    disable_trigger();
  }
  
  // check user bit 1 ... active enable SIS3820
#ifdef MVME_ACCESS
  if ((sis3820_RegisterRead(gVme,gMcsBase[0],SIS3820_CONTROL_STATUS) & 0x10000000)>>28 && (iMCSrun < 2))
#elif CAENCOMM_ACCESS
#endif
  {
    printf("user bit 1 active\n"); 
    iMCSrun = 2;
    enable_trigger();
  }
  
#ifdef MVME_ACCESS
  printf("polling MCS .... user bit = %x\n"
	 ,(sis3820_RegisterRead(gVme,gMcsBase[0],SIS3820_CONTROL_STATUS) & 0x10000000)>>28);    
  int reg = sis3820_RegisterRead(gVme,gMcsBase[0],SIS3820_FIFO_WORDCOUNTER);
#elif CAENCOMM_ACCESS
  uint32_t csr, reg;
  sCAEN= CAENComm_Read32(device_handle, gMcsBase[0]+SIS3820_CONTROL_STATUS, &csr);
  csr = (csr & 0x10000000)>>28;
  printf("polling MCS .... user bit = %x\n", csr);
  sCAEN= CAENComm_Read32(device_handle, gMcsBase[0]+SIS3820_FIFO_WORDCOUNTER, &reg);
#endif
  
  printf("SIS3820_FIFO_WORDCOUNTER %8.8x \n",reg);
  if ((int32_t)reg == (iBins*SIS3820_CHANNELS))
    {
      //iCycle++; // increase cycle count
      /* readout of scaler data */
#ifdef MVME_ACCESS
      int rcsum = sis3820_FifoRead(gVme,gMcsBase[0], data, SIS3820_CHANNELS*iBins) ;
#elif CAENCOMM_ACCESS
      int32_t rcsum;
      sCAEN = CAENComm_BLTRead(device_handle, gMcsBase[0]+SIS3820_FIFO_BASE, data, iBins*SIS3820_CHANNELS*sizeof(uint32_t), &rcsum); 
#endif
      //int rcsum = 1; 
      // int i,j,k;
      if(rcsum>0) {
	printf("sis3820_FifoRead OK\n\n");
	/*
	  k=0;
	  for (j=1;j<=iBins;j++) {
	  printf("scan: %3.3d ",j);
	  for (i=1;i<=SIS3820_CHANNELS;i++) {
	  //reg = sis3820_RegisterRead(myvme,sis3820_base,SIS3820_FIFO_BASE);
	  //printf("ch%2.2d %8.8x  ",i,reg);
	  printf("ch%2.2d %8.8x  ",i,data[k]);
	  k++;
	  }
	  printf("\n");
	  }  
	*/
      } else {
	printf("sis3820_FifoRead ERROR\n");
      }
      
      /* init bank structure */
      
      bk_init32(pevent);
      
      uint32_t *pdata32;
      bk_create(pevent, "MCMX", TID_DWORD, (void **) &pdata32);
      memcpy(pdata32,data,iBins*SIS3820_CHANNELS*4);
      pdata32 += SIS3820_CHANNELS*iBins;
      
      bk_close(pevent, pdata32);
      iMCSrun = 0;
      return bk_size(pevent);
      
    }
 }
 return 0;
 
#else
 return 0;
#endif
}

/*-- Trigger event routines ----------------------------------------*/
INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
//  gettimeofday(&now,NULL);
  int lam;
//  printf("poll_event %d %d %d!\n",source,count,test);

  for (int i=0 ; i<count ; i++)
  {
    #ifdef HAVE_SIS3820
#ifdef MVME_ACCESS
    if(((sis3820_RegisterRead(gVme,gMcsBase[0],SIS3820_CONTROL_STATUS) & 0x10000000)>>28) && bUserBit) {
      sis3820_RegisterWrite(gVme,gMcsBase[0],SIS3820_COUNTER_CLEAR,0xFF);  // clear counters 1-8
      bUserBit = FALSE;
    } else  {
      bUserBit = TRUE;
    }
#elif CAENCOMM_ACCESS
  uint32_t csr;
  sCAEN= CAENComm_Read32(device_handle, gMcsBase[0]+SIS3820_CONTROL_STATUS, &csr);
  csr = (((csr & 0x10000000)>>28) && bUserBit);  
  if (csr) bUserBit = FALSE; else bUserBit = TRUE;
#endif
    
#endif
#ifdef HAVE_V792
    //lam = v792_DataReady(gVme,gAdc1base);
    lam = 1;
    //printf("polling MCS .... user bit = %x\n",(sis3820_RegisterRead(gVme,gMcsBase[0],SIS3820_CONTROL_STATUS) & 0x10000000)>>28); 
    
    //int    lam = !(v792_CSR2Read(gVme, gAdc1base) & 0x2);
    //printf("source: 0x%x, lam: 0x%x\n", source, lam);
#else
    lam = 1;
#endif
    if (lam)
      if (!test) {
	// gettimeofday(&before,NULL);
	//double td = (-now.tv_sec + before.tv_sec) + 0.000001*(-now.tv_usec + before.tv_usec);
	//printf("poll_td = %f\n",td);
	return TRUE;
      }
  }
  
  return FALSE;
  
}
 
INT read_vme_event(char *pevent, INT off)
{
  static int gCountEmpty = 0;
  int size = 0;
  
  //printf("read_vme_event!\n");
  
#ifdef HAVE_SIS3820
  int *count;
  count = new int[kNumSisModules];
  
  // each SIS has how much data?
  for (int i=0; gMcsBase[i]; i++)
    count[i] = have_SIS_data(i);
  
  // which SIS has the least data
  int min = count[0];
  for (int i=1; gMcsBase[i]; i++)
    if (count[i] < min)
      min = count[i];
  
  //printf("SIS counts %d: %d %d, fifo %d %d\n", min, count[0], count[1], sis3820_DataReady(gVme,gMcsBase[0]), sis3820_DataReady(gVme,gMcsBase[1]));
  
#if 1
  if (min > 0)
    for (int i=0; gMcsBase[i]; i++)
      {
	if (!size)
	  bk_init32(pevent);
	size |= read_SIS(pevent, i, min);
      }
#endif
  
#endif
  
  if (size)
    {
      gCountEmpty = 0;
      gVmeIsIdle = 0;
      //printf("read_vme_event %d bytes\n", bk_size(pevent));
      return bk_size(pevent);
    }
  
  gCountEmpty++;
  
  if (gCountEmpty > 2) {
    gVmeIsIdle = 1;
    xusleep(1000);
  }
  
  return 0;
}

/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * tab-width: 8
 * c-basic-offset: 2
 * End:
 */
