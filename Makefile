# Makefile
#
# $Id$
#

MIDASSYS=$(HOME)/packages/midas

OSFLAGS  = -DOS_LINUX -Dextname
CFLAGS   = -fpermissive -g -O2 -fPIC -Wall -Wuninitialized -I. -I$(MIDASSYS)/include -std=c++11 
#CXXFLAGS = $(CFLAGS) -DHAVE_ROOT -DUSE_ROOT -I$(ROOTSYS)/include
CXXFLAGS = $(CFLAGS)

LIBS = -lm -lz -lutil -lnsl -lpthread -lrt

# MIDAS library
CFLAGS += -I$(MIDASSYS)/drivers/vme
MIDASLIBS = $(MIDASSYS)/lib/libmidas.a
MFE = $(MIDASSYS)/lib/mfe.o

CAENCOMM_DIR = $(HOME)/packages/CAENComm-1.02
CFLAGS += -I$(CAENCOMM_DIR)/include
CAENVME_DIR  = $(HOME)/packages/CAENVMELib-2.50
CFLAGS += -I$(CAENVME_DIR)/include
CFLAGS += -L$(CAENVME_DIR)/lib
CFLAGS += -L$(CAENCOMM_DIR)/lib
CFLAGS      += -DCAENCOMM_ACCESS


# ROOT library
ROOTGLIBS = $(shell $(ROOTSYS)/bin/root-config --glibs) -lThread -Wl,-rpath,$(ROOTSYS)/lib

# ROOT analyzer library
### ROOTANA = /home/fgddaq/packages/rootana
### CFLAGS += -I$(ROOTANA) -DHAVE_MIDAS 
### ROOTGLIBS += -lXMLParser

all: feyield

dox: ; doxygen

install: dox

feyield: %: %.o $(MIDASLIBS) $(MFE) sis3820.o v792.o 
	$(CXX) -o $@ $(CFLAGS) $(OSFLAGS) $^ $(MIDASLIBS) $(LIBS)

#strlcpy.o: $(MIDASSYS)/../mxml/strlcpy.c
#	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<

%.o: %.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c $<

%.o: $(MIDASSYS)/drivers/vme/%.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c $<

%.o: $(MIDASSYS)/drivers/vme/vmic/%.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c $<

client_main.o: %.o: %.c
	$(CXX) $(CXXFLAGS) $(OSFLAGS) -c $<

%.o: %.cxx
	$(CXX) $(CXXFLAGS) $(OSFLAGS) -I$(ROOTSYS)/include -c $<

clean::
	-rm -f *.o *.exe

# end
